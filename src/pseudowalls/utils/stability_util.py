from sympy import I
try:
    from sage.symbolic.constants import I
    def real_part(s):
        return s.real_part()
    def imag_part(s):
        return s.imag_part()
    # SAGE_INSTALLED = True
except:
    def real_part(s):
        return s.as_real_imag()[0]
    def imag_part(s):
        return s.as_real_imag()[1]
    # SAGE_INSTALLED = False

def stab_from_rank_degree(cls):
    class stab_class(cls):
        def central_charge(self, v):
            return (
            - self.degree(v)
            + I*self.rank(v)
            )

        def slope(self, v):
            return (
            self.degree(v)
            / self.rank(v)
            )

        def wall_eqn(self, v, w):
            return (
                self.degree(v) * self.rank(w)
                - self.degree(w) * self.rank(v)
            )

    return stab_class

def stab_from_central_charge(cls):
    class stab_class(cls):
        def rank(self, v):
            return imag_part(self.central_charge(v))

        def degree(self, v):
            return - real_part(self.central_charge(v))

        def slope(self, v):
            return self.degree(v)/self.rank(v)

        def wall_eqn(self, v, w):
            return (
                self.degree(v) * self.rank(w)
                - self.degree(w) * self.rank(v)
            )

    return stab_class