try:
    from sage.all import latex
    from sage.all import var as _sage_var
    from sage.symbolic.constants import I

    def symbols(*args, **kwargs):
        sage_kwargs = {}
        if "real" in kwargs and kwargs["real"] \
           or "rational" in kwargs and kwargs["rational"]:
            sage_kwargs["domain"] = "real"
        if "positive" in kwargs and kwargs["positive"]:
            sage_kwargs["domain"] = "positive"
        if "integer" in kwargs and kwargs["integer"]:
            sage_kwargs["domain"] = "integer"

        return _sage_var(*args, **sage_kwargs)
    
    def Symbol(*args, **kwargs):
        sage_kwargs = {}
        if "real" in kwargs and kwargs["real"] \
           or "rational" in kwargs and kwargs["rational"]:
            sage_kwargs["domain"] = "real"
        if "positive" in kwargs and kwargs["positive"]:
            sage_kwargs["domain"] = "positive"
        if "integer" in kwargs and kwargs["integer"]:
            sage_kwargs["domain"] = "integer"

        return _sage_var(*args, **sage_kwargs)
    
    SAGE_INSTALLED = True
    from sympy import factorial, Rational # Maybe find SageMath equivs
except:
    from sympy import Symbol, symbols, latex, factorial, Rational, I
    SAGE_INSTALLED = False