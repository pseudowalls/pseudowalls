# Pseudowalls

Personal working notebooks and symbolic algebra library for Python or SageMath to programmatically get expressions related to Bridgeland stability conditions and walls (Surfaces + 3-folds, Picard rank 1).

Inspired by Benjamin Schmidt's [library](https://github.com/benjaminschmidt/stability_conditions). For computations of the walls for surfaces, look there instead.

## Demos

|Features | SageMath Notebook | Python+Sympy Notebook|
|---------|-------------------|----------------------|
| Chern Characters |[webpage](https://pseudowalls.gitlab.io/pseudowalls/demo.sage.html) <br/> [notebook](demo/demo.sage.ipynb) | [webpage](https://pseudowalls.gitlab.io/pseudowalls/demo.sympy.html) <br/> [notebook](demo/demo.sympy.ipynb) |
| Stability Conditions | [webpage](https://pseudowalls.gitlab.io/pseudowalls/stability.sage.html) <br/> [notebook](demo/stability.sage.ipynb) | [webpage](https://pseudowalls.gitlab.io/pseudowalls/stability.sympy.html) <br/> [notebook](demo/stability.sage.ipynb)|

## Other

There's a list of all rendered notebooks [here](https://pseudowalls.gitlab.io/pseudowalls)

# Try it Yourself

With Python or SageMath installed you can install the library here as a [pip package](https://gitlab.com/pseudowalls/pseudowalls/-/packages/):

```bash
pip install pseudowalls --extra-index-url https://gitlab.com/api/v4/projects/43962374/packages/pypi/simple
```

```bash
sage -pip install pseudowalls --extra-index-url https://gitlab.com/api/v4/projects/43962374/packages/pypi/simple
```

The demo `.ipynb` files can be opened in Jupyter notebook (to be installed, should be included with SageMath).
